# (a)mazing

A tiny maze generator written in JavaScript based on classical algorithms: binary tree, sidewinder and backtracker.

![screenshot #1](screenshots/amazing1.png)

![screenshot #2](screenshots/amazing2.png)
